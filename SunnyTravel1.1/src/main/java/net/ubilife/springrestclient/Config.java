package net.ubilife.springrestclient;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(DataConfig.class)
@ComponentScan("net.ubilife.springrestclient")
public class Config {
	
}
